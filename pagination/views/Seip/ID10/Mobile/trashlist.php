<?php
if (isset($_SESSION['message']))
{
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
include_once ("../../../../vendor/autoload.php");
use App\Seip\ID10\Mobile\Mobile;
$obj = new Mobile();
$allData=$obj->trashed();
?>

<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<a href="create.php">add new</a>
<a href="index.php">orginial list</a>
<table>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>action</th>
    </tr>
    <?php
    $serial=1;
    foreach ($allData as $key=>$value)
    {
        ?>

        <tr>
            <td><?php echo $value['id'] ?></td>
            <td><?php echo $value['title'] ?></td>
            <td><a href="show.php?id=<?php echo $value['unique_id']?>">show</a>
                <a href="edit.php?id=<?php echo $value['unique_id']?>">edit</a>
                <a href="delete.php?id=<?php echo $value['unique_id']?>">delete</a>
                <a href="restore.php?id=<?php echo $value['unique_id']?>">restore</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
</body>
</html>