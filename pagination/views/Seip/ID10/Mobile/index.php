<?php

if (isset($_SESSION['message']))
{
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
include_once ("../../../../vendor/autoload.php");
use App\Seip\ID10\Mobile\Mobile;
$obj = new Mobile();
$itemperpage = 10;

if(!empty($_GET['page'])){
    $pageNumber = $_GET['page'];
}
else{
    $pageNumber=0;
}
if(!empty($_GET['page'])){
    $offset=$itemperpage*$_GET['page'];
}
else{
    $offset = 0;
}
$allData=$obj->index($itemperpage,$offset);
$totlaRows = $allData['totalrow'];
$numberofPage = ceil($totlaRows / $itemperpage);
echo "tatal row $totlaRows";
echo "total page $numberofPage";

array_pop($allData);
?>

<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<button style="color: red;">home</button>
<a href="create.php">add new</a>
<a href="trashlist.php">trash list</a>
<table>
    <tr>
        <th>serial</th>
        <th>id</th>
        <th>name</th>
        <th>action</th>
    </tr>
    <?php
    $serial=$itemperpage*$pageNumber;
    foreach ($allData as $key=>$value)
    {
        ?>

        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $value['id'] ?></td>
            <td><?php echo $value['title'] ?></td>
            <td><a href="show.php?id=<?php echo $value['unique_id']?>">show</a>
                <a href="edit.php?id=<?php echo $value['unique_id']?>">edit</a>
                <a href="trash.php?id=<?php echo $value['unique_id']?>">delete</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<?php
for ($i = 0;$i<$numberofPage;$i++){
    echo "<a href=index.php?page=$i>".$i."-"."</a>";
}
?>
</body>
</html>