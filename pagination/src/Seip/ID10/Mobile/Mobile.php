<?php
namespace  App\Seip\ID10\Mobile;
use PDO;
class Mobile{
    public $id='';
    public $title='';
    public $pdo='';

    public function __construct()
    {
        $this->pdo= new PDO ('mysql:host=localhost;dbname=php39','root','');
        session_start();
    }

    public function setData($data="")
    {
        if(array_key_exists('mobile_model',$data)) {
            $this->title = $data['mobile_model'];
        }
        if(array_key_exists('id',$data)) {
            $this->id=$data['id'];
        }

        return $this;
    }
    public function index($itemperpage="",$offset="")
    {
        try{

//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
            $query2 = "SELECT SQL_CALC_FOUND_ROWS * FROM `mobiles` where `deleted_at`='0000-00-00 00:00:00' ORDER BY id DESC LIMIT $itemperpage  OFFSET $offset";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute();
            $data = $stmt->fetchAll();
            $totalrows = $this->pdo->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $data['totalrow']=$totalrows;
            return $data;

        }
        catch (PDOException $e){
            echo 'Error:'. $e->getMessage();
        }
    }
    public function show()
    {
        try {
//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
            $query2 = "SELECT * FROM `mobiles` WHERE unique_id="."'".$this->id."'";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        }

        catch (PDOException $e){
            echo 'Error:'. $e->getMessage();
        }
    }

 public function store()
 {
    try{
//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
        $query2 = "INSERT INTO `mobiles`(`id`,`unique_id`,`title`) VALUES (:id,:unique_id,:title)";
        $stmt = $this->pdo->prepare($query2);
           $stmt->execute(
               array(
                   ':id'=> null,
                   ':unique_id'=>uniqid(),
                   ':title'=>$this->title,

               )
           );
           if ($stmt){

               $_SESSION['message']="Successfully Added";
               header('location:create.php');
           }
    }
    catch (PDOException $e){
        echo 'Error:'. $e->getMessage();
    }
 }
 public function update()
 {

     try{
//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
         $query2 = "UPDATE  `mobiles` SET title=:title WHERE id=:id";
         $stmt = $this->pdo->prepare($query2);
         $stmt->execute(
             array(
                 ':id'=> $this->id,
                 ':title'=>$this->title
             )
         );
         if ($stmt){
             $_SESSION['message']="Successfully Updated";
             header('location:index.php');
         }
     }
     catch (PDOException $e){
         echo 'Error:'. $e->getMessage();
     }
 }
 public function delete()
 {
     try{

//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
         $query = "DELETE FROM `mobiles` WHERE `mobiles`.unique_id="."'".$this->id."'";
         $stmt = $this->pdo->query($query);
         $stmt->execute();
         if ($stmt){
             $_SESSION['message']="Successfully Deleted";
             header('location:trashlist.php');

         }

     }
     catch (PDOException $e){
         echo 'Error:'. $e->getMessage();
     }
 }
 public function trash()
 {
     try{

//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
         $query2 = "UPDATE  `mobiles` SET deleted_at = :delete WHERE unique_id="."'".$this->id."'";
         $stmt = $this->pdo->prepare($query2);
         $stmt->execute(
             array(
                 ':delete'=>date('y-m-d h:m:s'),

             )
         );
         if ($stmt){
             $_SESSION['message']="Successfully Updated";
             header('location:index.php');
         }
     }
     catch (PDOException $e){
         echo 'Error:'. $e->getMessage();
     }
 }
 public function trashed()
 {
     try{
//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
         $query2 = "SELECT * FROM `mobiles` where `deleted_at`!='0000-00-00 00:00:00'";
         $stmt = $this->pdo->prepare($query2);
         $stmt->execute();
         $data = $stmt->fetchAll();
         return $data;

     }
     catch (PDOException $e){
         echo 'Error:'. $e->getMessage();
     }
 }
    public function restore()
    {
        try{

//        $query1= "INSERT INTO `mobiles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'iphne7', '', '', '')";
            $query2 = "UPDATE  `mobiles` SET deleted_at = :delete WHERE unique_id="."'".$this->id."'";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(
                array(
                    ':delete'=>'0000-00-00 00:00:00',

                )
            );
            if ($stmt){
                $_SESSION['message']="Successfully Restore";
                header('location:index.php');
            }
        }
        catch (PDOException $e){
            echo 'Error:'. $e->getMessage();
        }
    }


}