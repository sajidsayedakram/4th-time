<?php

namespace App\id\Mobile;
use PDO;
class Mobile
{
    private $id = "";
    private $title = "";
    private $hobbies = "";
    private $pdo = "";
    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=sajidlab','root','');
    }
    public function setData($data = '')
    {
       if (array_key_exists('name',$data)) {
           $this->title = $data['name'];
       }
        if (array_key_exists('id',$data)) {
           $this->id = $data['id'];
       }if (array_key_exists('hobbies',$data)) {
           $this->hobbies = $data['hobbies'];
       }
        return $this;
    }//End of setData
    public function index($perpage='',$offset=''){
        try{
            $query = "SELECT  SQL_CALC_FOUND_ROWS * FROM `examcrud` WHERE `delected_at` = '0000-00-00 00:00:00'ORDER BY id DESC LIMIT $perpage OFFSET $offset";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            $subquery = "SELECT FOUND_ROWS()";
            $totalrow = $this->pdo->query($subquery)->fetch(PDO::FETCH_COLUMN);
            $value['totalrow'] = $totalrow;
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of index
    public function show(){
        try{
            $query = "SELECT * FROM `examcrud` WHERE id =".$this->id;
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of show
//    public function update()
//    {
//        try {
//            $query = 'UPDATE sometable SET title = :mobile_brand WHERE id = :id';
//            $stmnt = $this->pdo->prepare($query);
//            $stmnt->execute(
//                array(
//                    ':id' => $this->id,
//                    ':mobile_brand' => $this->title
//                )
//            );
//            if ($stmnt) {
//                $_SESSION['message'] = "Successfully Data Updated";
//                header('location:listbrand.php');
//            }
//        }
//        catch (PDOException $e) {
//            echo  'Error' . $e->getMessage();
//        }
//    }//End of update
    public function trash()
    {
        try {
            $query = 'UPDATE examcrud SET delected_at = :delete_time WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->id,
                    ':delete_time' => date('Y-m-d h:m:s')
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header('location:listbrand.php');
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Trash
    public function trashted()
    {
        try{
            $query = "SELECT * FROM `examcrud` WHERE `delected_at` != '0000-00-00 00:00:00'";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Trashted
    public function restore()
    {
        try {
            $query = 'UPDATE examcrud SET delected_at = :delete_time WHERE id = :id';
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => $this->id,
                    ':delete_time' => '0000-00-00 00:00:00'
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Restore";
                header('location:trashlist.php');
            }
        }
        catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of Trash
    public function delete(){
        try{
            $query = "DELETE FROM `examcrud` WHERE `examcrud`.`id` =".$this->id;
            $stmnt = $this->pdo->query($query);
            $stmnt->execute();
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header('location:trashlist.php');
            }
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }//End of delete
    public function store()
    {
        try {
            $query = "INSERT INTO `examcrud` (`id`, `name`, `hobbies`) VALUES (:id,:title,:hobbies)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':title' => $this->title,
                    ':hobbies' => $this->hobbies
                )
            );
            if ($stmnt) {
                $_SESSION['mes'] = "Successfully Data Added";
                header('location:create.php');
            }
        }catch (PDOException $e ) {
        echo 'Error' . $e->getMessage();
        }


    }//End of store

}
