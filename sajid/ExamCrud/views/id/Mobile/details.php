<?php
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;

$obj = new Mobile();
$obj->setData($_GET);
$value = $obj->show();
?>
<html>
<head>
    <title>Add Person</title>

</head>

<body class="indbody">
<h1><a href="create.php">Add Person</a> | <a href="listbrand.php">List name</a></h1>
<table border="1" align="center">
    <tr>
        <td>Serial No.</td>
        <td>Name</td>
        <td>Hobbies Name</td>
        <td align="center">Action</td>
    </tr>
        <tr>
            <td><?php echo "1"; ?></td>
            <td><?php echo $value['name']; ?></td>
            <td><?php echo $value['hobbies']; ?></td>
            <td>
                <a href="edit.php?id=<?php echo $value['id']; ?>">Edit</a>
                <a href="trash.php?id=<?php echo $value['id']; ?>">Delete</a>
            </td>
        </tr>
</table>
</div>
</body>
</html>