<?php
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;

$str = implode(",",$_POST['chk_group']);
$_POST['hobbies'] = $str;

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (preg_match("/([a-zA-Z0-9])/",$_POST['name'])){
        $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING );
        $mobile1 = new Mobile();
        $mobile1->setData($_POST)->store();
    }else {
        $_SESSION['message'] = "Invalid Input";
        header("location:create.php");
    }
}else {
    header("location:create.php");
}
