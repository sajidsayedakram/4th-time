<?php
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;

$obj = new Mobile();
$obj->setData($_GET);
$value = $obj->show();

//$str = implode(",",$_POST['chk_group']);
//$_POST['hobbies'] = $str;
?>
<html>
<head>
    <title>Add hobbies </title>
</head>

<body >

<div>
    <fieldset>
        <legend><a href="index.php">Home</a> | Database  | <a href="listbrand.php">List Name</a></legend>
        <form action="update.php" method="post" >
            Name : <input type="text" name="name"required value="<?php echo $value['name'];?>" autofocus><br><br />

            Hobbies :<br>
            <input type="checkbox" name="chk_group[]" value="Learning" />Learning<br>
            <input type="checkbox" name="chk_group[]" value="Sports" />Sports<br>
            <input type="checkbox" name="chk_group[]" value="Singing"/>Singing<br>
            <input type="checkbox" name="chk_group[]" value="Nothing" />Nothing<br /><br />
            <input type="hidden" name="id" value="<?php echo $value['id'];?>">

            <input type="submit" value="Update">
        </form>
        <div align="center" id="autohide">
            <?php
            if (isset($_SESSION['message'])){
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            } ?>
        </div>
    </fieldset>
</div>
</body>
</html>
