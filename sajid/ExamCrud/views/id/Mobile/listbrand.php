<?php
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;
$perpage = 5;
if (!empty($_GET['page'])){
    $pagenumber = $_GET['page'];
}else{
    $pagenumber = 0;
}
if (!empty($_GET['page'])){
    $offset = $perpage * $pagenumber;
}else{
    $offset = 0;
}

$obj = new Mobile();
$value = $obj->index($perpage,$offset);
$totalrow = $value['totalrow'];
$numberofpage = ceil($totalrow/$perpage);
array_pop($value);
?>
<html>
<head>
    <title>Add</title>

</head>

<body>

Mobile Brand <a href="index.php">Home</a> | <a href="create.php">Add Person</a> | <a href="trashlist.php">Trash List</a>
<div  id="autohide">
    <?php
    if (isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
</div>
<br/>
<div ><?php echo "Total Row : ".$totalrow; echo "Total Page : ".$numberofpage; ?></div><br>
<table border="1">
    <tr>
        <td>Serial No.</td>
        <td>Brand-Name</td>
        <td>Action</td>
    </tr>
    <?php $serial = 1 ; foreach ($value as $key => $item ){ ?>
        <tr>
            <td><?php echo $serial++ ?></td>
            <td><?php echo $item['name']; ?></td>
            <td>
                <a href="details.php?id=<?php echo $item['id']; ?>">Show</a>
                <a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a>
                <a href="trash.php?id=<?php echo $item['id']; ?>">Delete</a>
            </td>
        </tr>
    <?php } ?>
</table><br><br>
<div> <?php
    echo "Pagination : ";
    for ($i = 0; $i < $numberofpage; $i++)
    {
        echo '<a href="listbrand.php?page='.$i.'">'.$i.",".'</a>';
    }
    ?> </div>
</body>
</html>