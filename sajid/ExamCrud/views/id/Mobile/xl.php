<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_USER_DEPRECATED);
ini_set('display_errors',TRUE);
ini_set('display_startup_errors',TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI == 'cli')
    die('This example should only be run from aweb browser');
/** Include PHpExcel  */
include_once("../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php");
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;

$obj = new Mobile();
$value = $obj->index();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
//Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliaum")
    ->setLastModifiedBy("Maarten Balliaum")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test Document for Office 2007")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
//Add some data

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL')
    ->setCellValue('B1', 'ID')
    ->setCellValue('C1', 'Brand Name');
$counter=2;
$serial=0;

foreach ($value as $item ) {
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $counter, $serial)
        ->setCellValue('B' . $counter, $item['id'])
        ->setCellValue('C' . $counter, $item['title']);
    $counter++;
}
// Rename Worksheet
$objPHPExcel->getActiveSheet()->setTitle('Mobile_list');
// Set active sheet index to the first sheet,so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Mobile Brand List.xls"');
header('Content-Control: max-age=0');
header('Content-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header('Pragma:public');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;