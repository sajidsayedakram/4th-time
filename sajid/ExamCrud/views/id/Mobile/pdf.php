<?php
include('../../../vendor/mpdf/mpdf/mpdf.php');
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;

$obj = new Mobile();
$value = $obj->index();
$trs ="";
$serial = 0 ;
foreach ($value as $item ):
$serial++;
$trs.="<tr>";
$trs.="<td>".$serial."</td>";
$trs.="<td>".$item['id']."</td>";
$trs.="<td>".$item['title']."</td>";
$trs.="</tr>";
endforeach;

$html = <<<EOD
<!DOCTYPE html>

<head>
    <title>Add Mobile Brand</title>
    <link href="../../../default.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript"> $(function() { setTimeout(function() { $("#autohide").fadeOut(1500); }, 3000)}) </script>
</head>

<body class="indbody">
<h1>Mobile Brand <a href="index.php">Home</a> | <a href="create.php">Add Brand</a> | <a href="trashlist.php">Trash List</a></h1>
<br/>
<table border="1" align="center">
    <tr>
        <td>Serial No.</td>
        <td>ID No.</td>
        <td>Brand-Name</td>
    </tr>
   $trs;
</table>
<br/><br/><br/>
<div align="center" > Powered By INDRO </div>
</body>
</html>
EOD;

$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
