<?php
include_once ("../../../vendor/autoload.php");
use App\id\Mobile\Mobile;
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (preg_match("/([a-zA-Z0-9])/",$_POST['mobile_brand'])){
        $_POST['mobile_brand'] = filter_var($_POST['mobile_brand'], FILTER_SANITIZE_STRING );
        $mobile1 = new Mobile();
        $mobile1->setData($_POST)->update();
    }else {
        $_SESSION['message'] = "Invalid Input";
        header("location:create.php");
    }
}else {
    header("location:create.php");
}
