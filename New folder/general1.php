<?php
include_once("../Src/form.php");
$obj = new form();
$obj->setData($_GET);
$data=$obj->form();
//echo "<pre>";
//print_r($data);
//die();

?>

<!DOCTYPE html>
<html lang="en">
<?php
?>
<?php
include_once("../include/head2.php")
?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
      include_once("../include/header.php")
      ?>
      <!--header end-->

      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <?php
      include_once("../include/sidebar.php")
      ?>
      <!--sidebar end-->

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
      		<div class="row mt">
          <table class="table table-bordered table-hover table-striped">
              <thead>
              <tr>
                  <th>ID</th>
                  <th>Country Code</th>
                  <th>User Name</th>
                  <th>Email</th>
              </tr>
             <?php
                            $serial = 1;
                            foreach ($data as $key => $value){ ?>

              <tr>
                  <td><?php echo $serial++ ?></td>
                  <td><?php echo $value ['country_code']?></td>
                  <td><?php echo $value ['name'] ?></td>
                  <td><?php echo $value ['email'] ?></td>
              </tr>
         <?php } ?>
              </tbody>
          </table>


      </div><!--/ row -->
      </section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <?php
      include_once("../include/footer.php")
      ?>
      <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php
  include_once("../include/script.php")
  ?>
  </body>
</html>
