<?php
include_once ("../../../../vendor/autoload.php");
use App\Seip\ID10\Mobile\Mobile;
if($_SERVER['REQUEST_METHOD']=='POST') {
    if (!empty($_POST['mobile_model']))
    {
        if (preg_match("/([a-zA-Z0-9_])/",$_POST['mobile_model']))
        {
            $_POST['mobile_model'] = filter_var($_POST['mobile_model'],FILTER_SANITIZE_STRING);
            $mobile1 = new mobile();
            $mobile1->setData($_POST)->update();
        }
        else
        {
            $_SESSION['message']="invalid input";
            header('location:create.php');
        }

    }
    else{
        $_SESSION['message']="input can't empty";
        header('location:create.php');
    }
}
else{

    $_SESSION['message']="Input can't be empty";
    header("location:create.php");
}