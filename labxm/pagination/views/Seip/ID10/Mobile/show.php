<?php
include_once ("../../../../vendor/autoload.php");
use App\Seip\ID10\Mobile\Mobile;
$obj = new Mobile();
$obj->setData($_GET);
$data = $obj->show();
?>

<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<a href="create.php">add new</a>
<a href="index.php">back list</a>
<table>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>action</th>
    </tr>

        <tr>
            <td><?php echo $data['id'] ?></td>
            <td><?php echo $data['title'] ?></td>

        </tr>

</table>
</body>
</html>
