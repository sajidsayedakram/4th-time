<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<?php
include_once("include/head.php")
?>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

      <div>
          <div class="container">

              <form class="form-login" action="views/login.php" method="post" >

		        <h2 class="form-login-heading">sign in Admin</h2>
                  <?php
                  echo '<h4 style="color:green">';
                  if (isset($_SESSION['message'])) {
                      echo $_SESSION['message'];
                      unset($_SESSION['message']);
                  }
                  echo '</h4>';
                  echo '<h4 style="color:red">';

                  if (isset($_SESSION['fail'])) {
                      echo $_SESSION['fail'];
                      unset($_SESSION['fail']);
                  }
                  echo '</h4>';
                  ?>
		        <div class="login-wrap">
		            <input type="email" name="email" class="form-control" placeholder="You Email" >
		            <br>
		            <input type="password" name="password" class="form-control" placeholder="Password">
                    <br>
		            <button class="btn btn-theme btn-block"><i class="fa fa-lock"></i> SIGN IN</button>
		            <hr>

		            <div class="login-social-link centered">

		            <p>or you can sign in via your social network</p>
		                <button class="btn btn-facebook" ><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" ><i class="fa fa-twitter"></i> Twitter</button>
		            </div>
		            <div class="registration">
		                Don't have an account yet?<br/>
		                <a class="" href="">
		                    Create an account
		                </a>
		            </div>

		        </div>


		      </form>
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>

