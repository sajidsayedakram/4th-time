<?php
include_once("../Src/forms6.php");
$obj = new forms6();
$obj->setData($_GET);
$data=$obj->form();
//echo "<pre>";
//print_r($data);
//die();

?>

<?php
//session_start();
if (!empty($_SESSION['user_info']))


{ ?>

<!DOCTYPE html>
<html lang="en">
<?php
?>
<?php
include_once("../include/head2.php")
?>

<body>

<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php
    include_once("../include/header.php")
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->

    <?php
    include_once("../include/sidebar.php")
    ?>
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <section class="wrapper">
                      <h3><i class="fa fa-angle-right"></i> Page 6</h3>
                      <div class="row">

                          <div class="col-md-12">
                              <div class="content-panel">
                                  <h4><i class="fa fa-angle-right"></i> Basic Table</h4>
                                  <hr>
                                  <table class="table">
                                      <thead>
                                      <tr>
                                          <th>ID </th>
                                          <th>Country Code</th>
                                          <th>Name</th>
                                          <th>Email</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $serial = 1;
                                      foreach ($data as $key => $value){ ?>

                                          <tr>
                                              <td><?php echo $serial++ ?></td>
                                              <td><?php echo $value ['country_code']?></td>
                                              <td><?php echo $value ['name'] ?></td>
                                              <td><?php echo $value ['email'] ?></td>
                                          </tr>
                                      <?php } ?>
                                      </tbody>
                                  </table>
                              </div><! --/content-panel -->
                          </div><!-- /col-md-12 -->
                      </div><!-- row -->

                  </section><! --/wrapper -->


		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->


      <!--main content end-->
    <!--footer start-->
    <?php
    include_once("../include/footer.php")
    ?>
    <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<?php
include_once("../include/blank_page_script.php")
?>
</body>
</html>
<?php } else {
    session_start();
    $_SESSION['fail'] = "Sorry ! You're not authorieze to accesss this page";
    header('location:../index.php');
}
?>