<?php
include_once("../Src/form.php");
$obj = new form();
$obj->setData($_GET);
$data=$obj->form();
//echo "<pre>";
//print_r($data);
//die();

?>
<?php
//session_start();
if (!empty($_SESSION['user_info']))


{ ?>
<!DOCTYPE html>
<html lang="en">
<?php
?>
<?php
include_once("../include/head2.php")
?>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <?php
      include_once("../include/header.php")
      ?>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->

      <?php
      include_once("../include/sidebar.php")
      ?>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Basic Table Examples</h3>
				<div class="row">
				
	                  <div class="col-md-12">
	                  	  <div class="content-panel">
	                  	  	  <h4><i class="fa fa-angle-right"></i> Basic Table</h4>
	                  	  	  <hr>
		                      <table class="table">
		                          <thead>
		                          <tr>
		                              <th>ID</th>
		                              <th>Country Code</th>
		                              <th>Last Name</th>
		                              <th>Username</th>
		                          </tr>
		                          </thead>
		                          <tbody>
		                          <tr>
		                              <td>1</td>
		                              <td>Mark</td>
		                              <td>Otto</td>
		                              <td>@mdo</td>
		                          </tr>
		                          <tr>
		                              <td>2</td>
		                              <td>Jacob</td>
		                              <td>Thornton</td>
		                              <td>@fat</td>
		                          </tr>
		                          <tr>
		                              <td>3</td>
		                              <td>Larry</td>
		                              <td>the Bird</td>
		                              <td>@twitter</td>
		                          </tr>
		                          </tbody>
		                      </table>
	                  	  </div><! --/content-panel -->
	                  </div><!-- /col-md-12 -->
				</div><!-- row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--footer start-->
      <?php
      include_once("../include/footer.php")
      ?>
      <!--footer end-->
  </section>

  <!-- js placed at the end of the document so the pages load faster -->
  <?php
  include_once("../include/script.php")
  ?>
  </body>
</html>
<?php } else {
    session_start();
    $_SESSION['fail'] = "Sorry ! You're not authorieze to accesss this page";
    header('location:../index.php');
}
?>
