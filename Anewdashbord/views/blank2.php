<?php
include_once("../Src/form.php");
$obj = new form();
$obj->setData($_GET);
$data=$obj->form();
//echo "<pre>";
//print_r($data);
//die();

?>
<?php
//session_start();
if (!empty($_SESSION['user_info']))


{ ?>

<!DOCTYPE html>
<html lang="en">
<?php
?>
<?php
include_once("../include/head2.php")
?>

<body>

<section id="container" >
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php
    include_once("../include/header.php")
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->

    <?php
    include_once("../include/sidebar.php")
    ?>
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Blank Page</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		<p>Place your content here.</p>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
    <!--footer start-->
    <?php
    include_once("../include/footer.php")
    ?>
    <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<?php
include_once("../include/blank_page_script.php")
?>
</body>
</html>
<?php } else {
    session_start();
    $_SESSION['fail'] = "Sorry ! You're not authorieze to accesss this page";
    header('location:../index.php');
}
?>