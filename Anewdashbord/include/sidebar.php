<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">

            <p class="centered"><a href="../views/dashboard.php"><img src="../assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
            <h5 class="centered">Marcel Newman</h5>

            <li class="mt">
                <a class="active" href="dashboard.php">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href=javascript:;" >
                    <i class="fa fa-desktop"></i>
                    <span>UI Elements</span>
                </a>
                <ul class="sub">
                    <li><a  href="../views/general.php">page 1</a></li>
                    <li><a  href="../views/general1.php">Page 2</a></li>
                    <li><a  href="../views/general2.php">Page 3</a></li>
                    <li><a  href="../views/general3.php">Page 4</a></li>
                    <li><a  href="../views/general4.php">Page 5</a></li>
                    <li><a  href="../views/general5.php">Page 6</a></li>


                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-cogs"></i>
                    <span>Components</span>
                </a>
                <ul class="sub">
                    <li><a  href="../views/calendar.php">Calendar</a></li>

                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-book"></i>
                    <span>Extra Pages</span>
                </a>
                <ul class="sub">
                    <li><a  href="../views/blank1.php">Blank Page 1</a></li>
                    <li><a  href="../views/blank2.php">Blank Page 2</a></li>
                    <li><a  href="../views/lock_screen.php">Lock Screen</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-th"></i>
                    <span>Data Tables</span>
                </a>
                <ul class="sub">
                    <li><a  href="../views/basic_table.php">Basic Table</a></li>

                </ul>
            </li>


        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>