<?php
include_once("../../../../vendor/autoload.php");

use App\seip\id10\name\name;

if($_SERVER['REQUEST_METHOD']=='POST'){
     if(!empty($_POST['User_name'])){
       if(preg_match("/([a-zA-Z0-9_])/",$_POST['User_name'])){

           $_POST['User_name'] =filter_var($_POST['User_name'],FILTER_SANITIZE_STRING);
           $mobile1  = new name();
           $mobile1->setData($_POST)->store();
       }
       else{
           session_start();
           $_SESSION['message'] = "Invalid Input";
           header("location:create.php");
       }

     } else{
         session_start();
    $_SESSION['message'] = "Input can't be empty";
    header("location:create.php");
}

} else{
    header("location:create.php");
}
