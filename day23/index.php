<?php
session_start();
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>
<html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php
include_once("include/head.php")
?>
<body>
<div class="container">
    <header>
        <h1> Admin </span></h1>


        <?php
        echo '<h2 style="color:green">';
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        echo '</h2>';
        echo '<h2 style="color:red">';

        if (isset($_SESSION['fail'])) {
            echo $_SESSION['fail'];
            unset($_SESSION['fail']);
        }
        echo '</h2>';
        ?>


    </header>
    <section>
        <div id="container_demo">
            <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
            <a class="hiddenanchor" ></a>
            <a class="hiddenanchor" ></a>

            <div id="wrapper">
                <div id="login" class="animate form">
                    <form action="views/login.php" method="post">
                        <h1>Log in</h1>

                        <p>
                            <label for="username" class="uname" data-icon="u"> Your email </label>
                            <input id="username" name="email" required="required" type="text"
                                   placeholder=" Your email"/>
                        </p>

                        <p>
                            <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                            <input id="password" name="password" required="required" type="password"
                                   placeholder="Inter your password"/>
                        </p>

                        <p class="login button">
                            <input type="submit" value="Login"/>
                        </p>

                    </form>
                </div>


            </div>
        </div>
    </section>
</div>
</body>
</html>