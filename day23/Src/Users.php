<?php
class Users
{
    private $id = '';
    private $username;
    private $email;
    private $password;
    private $pdo = '';


    public function __construct()
    {
//        echo "this output from users class";
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=php39', 'root', '');
    }

    public function setData($data = '')
    {
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }

        return $this;

    }



    public function login()
    {
        try {
            $query = "SELECT  * FROM `users` where email='$this->email' AND password=$this->password";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();

            if (empty($data)) {
                $_SESSION['fail'] = "Opps! Invalid email or password";
                header('location:../index.php');
            } else {
                $_SESSION['user_info'] = $data;
                header('location:../views/dashboard.php');
            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

}









