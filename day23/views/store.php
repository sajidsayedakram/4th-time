<?php
include_once("../Src/Users.php");

$obj = new Users();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        {
        $available = $obj->setData($_POST)->validity();
        if (empty($available)) {
            $obj->setData($_POST)->store();
        } else {
            $_SESSION['fail'] = "Username or Email already exists";
            header('location:../index.php#toregister');
        }
    }

} else {
    $_SESSION['fail'] = "You're not authorized to access this page";
    header('location:../index.php');
}
